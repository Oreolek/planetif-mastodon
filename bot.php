<?php
require_once ("vendor/autoload.php");
use Symfony\Component\Yaml\Yaml;
use Mremi\UrlShortener\Model\Link;
use Mremi\UrlShortener\Provider\Bitly\BitlyProvider;
use Mremi\UrlShortener\Provider\Bitly\GenericAccessTokenAuthenticator;
use Mremi\UrlShortener\Exception\InvalidApiResponseException;
use Revolution\Mastodon\MastodonClient;
use \GuzzleHttp\Client as GuzzleClient;
use Mekras\Atom\Document\FeedDocument;
use Mekras\Atom\DocumentFactory;
use Mekras\Atom\Exception\AtomException;

$config = Yaml::parse(file_get_contents('config.yml'));
$lastrun = 0;
if (file_exists('.lastrun') && !$config['DRY_RUN']) {
  $lastrun = file_get_contents('.lastrun');
}

function get_text($url) {
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_HEADER => FALSE,
    CURLOPT_URL => $url,
    CURLOPT_PROXYPORT => 17622,
    CURLOPT_PROXY => '10.8.0.1',
  ));
  $resp = curl_exec($curl);
  curl_close($curl);
  if ($resp === FALSE) {
    echo 'Error';
    die();
  }
  return $resp;
}

function ellipse($str,$n_chars,$crop_str=' [...]')
{
  $str = trim($str);
  $buff = strip_tags($str);
  if(strlen($buff) > $n_chars)
  {
    $cut_index=strpos($buff,' ',$n_chars);
    $buff=substr($buff,0,($cut_index===false? $n_chars: $cut_index+1)).$crop_str;
  }
  return $buff;
}

echo 'Downloading feed.'.PHP_EOL;
$string = get_text('https://planet-if.com/atom.xml');
echo 'Got feed.'.PHP_EOL;
$factory = new DocumentFactory();
try {
  $document = $factory->parseXML($string);
  unset($string);
} catch (AtomException $e) {
  die($e->getMessage());
}
$feed = $document->getFeed();
$articles = $feed->getEntries();
$pandoc = new \Pandoc\Pandoc();
$bitlyProvider = new BitlyProvider(
  new GenericAccessTokenAuthenticator($config['BITLY_TOKEN'])
);
$guzzle = new GuzzleClient([
  'timeout' => 30,
]);
$mastodon = new MastodonClient($guzzle);

foreach ($articles as $article) {
  if (strtotime($article->getUpdated()) <= $lastrun) {
    continue;
  }
  $title = (string) $article->getTitle();
  $link = new Link;
  $link->setLongUrl((string) $article->getId());
  try {
    $bitlyProvider->shorten($link);
  } catch (InvalidApiResponseException $e) {
    echo $e->getMessage();
    echo "Link: ".$article->getId();
  }
  $link = $link->getShortUrl();
  $description = (string) $article->getContent();
  $image = NULL;
  preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $description, $image);
  if (isset($image[1])) {
    $image = $image[1];
  }
  $description = strip_tags($description, 'img');
  $description = $pandoc->convert($description, "html", "plain");

  $limit = 500 - strlen($link) - strlen($title) - 20;
  $mdescription = "$title\n\n".ellipse($description, $limit);
  $mdescription .= "\n$link";
  if (!$config['DRY_RUN']) {
    if ($config['MASTODON'] === true) {
      $mastodon->domain($config['MASTODON_SERVER'])->token($config['MASTODON_ACCESS_TOKEN']);
      $mastodon->createStatus($mdescription, [
        'language' => 'en'
      ]);
    }
  } else {
    echo $mdescription.PHP_EOL;
  }
}
if (!$config['DRY_RUN']) {
  file_put_contents('.lastrun', time());
}
